package com.magnus.blouses.neckdesigs

class CustomException(message: String) : Exception(message)
